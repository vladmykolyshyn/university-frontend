export function getTokenFromLocalStorage(): string {
  const data = localStorage.getItem("accessToken");
  const accessToken: string = data ? JSON.parse(data) : "";

  return accessToken;
}

export function setTokenToLocalStorage(key: string, accessToken: string): void {
  console.log(`Setting token in localStorage for key ${key}: ${accessToken}`);
  localStorage.setItem(key, JSON.stringify(accessToken));
}

export function removeTokenFromLocalStorage(key:string):void {
    localStorage.removeItem(key)
}