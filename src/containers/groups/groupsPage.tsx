import React, { useEffect, useState } from 'react';
import './groupsPage.scss';
import Header from '../../components/header/header';
import plusImage from '../../img/Plus-icon.svg';
import { EntityService } from '../../services/entity.service';
import { IGroupResponse } from '../../types/types';
import GroupsCreateModal from '../../components/groups-modal-create/groupsCreateModal';
import GroupsUpdateModal from '../../components/groups-modal-update/groupsUpdateModal';
import { useAppDispatch } from '../../store/hooks';
import { setGroups } from '../../store/groups/groupSlice';

export const groupLoader = async () => {
  const response = await EntityService.getAllGroups();
  return response;
};

const GroupsPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const [groups, setGroupData] = useState<IGroupResponse[]>([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  console.log(groups);

  const loadGroups = async () => {
    const response = await EntityService.getAllGroups();
    setGroupData(response);
    dispatch(setGroups(response));
    setDataLoaded(true);
  };

  useEffect(() => {
    if (!dataLoaded) {
      loadGroups();
    }
  }, []);

  const [groupId, setGroupId] = useState<number>(0);
  const [groupName, setGroupName] = useState<string | null>('');
  const [visibleModalGroup, setVisibleModalGroup] = useState<boolean>(false);
  const [visibleUpdateModalGroup, setVisibleUpdateModalGroup] = useState<boolean>(false);

  return (
    <section className="groups-container">
      <Header title="Groups" />
      <div className="actions-group">
        <button
          className="add-group-btn"
          onClick={() => setVisibleModalGroup(true)}
        >
          <img src={plusImage} alt="plus" />
          <span>Add new group</span>
        </button>
      </div>
      {/* Header of table */}
      <div className="group-table-header">
        <div className="group-column-header group-column-header-first">
          Name
        </div>
        <div className="group-column-header"></div>
      </div>
      <div className="groups-functionality">
        {/* ModalWindow for create group */}
        {visibleModalGroup && (
          <GroupsCreateModal
            setVisibleModalGroup={setVisibleModalGroup}
            updateGroupsList={loadGroups}
          />
        )}
        {/* ModalWindow for update group */}
        {visibleUpdateModalGroup && (
          <GroupsUpdateModal
            id={groupId}
            name={groupName}
            setVisibleUpdateModalGroup={setVisibleUpdateModalGroup}
            updateGroupsList={loadGroups}
          />
        )}
        {/* Table */}
        <div className="group-table-container">
          <div className="group-table">
            <div className="group-table-body">
              {groups.map((group) => (
                <div className="group-row" key={group.id}>
                  <div className="group-column-table group-column-table-first">
                    {group.name}
                  </div>
                  <div className="group-column-table group-column-table-last">
                    <button
                      onClick={() => {
                        setGroupId(group.id);
                        setGroupName(group.name);
                        setVisibleUpdateModalGroup(true);
                      }}
                    ></button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default GroupsPage;
