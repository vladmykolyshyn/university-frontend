import React, { useEffect, useState } from 'react';
import './lectorsPage.scss';
import Header from '../../components/header/header';
import plusImage from '../../img/Plus-icon.svg';
import { EntityService } from '../../services/entity.service';
import { ICreateLectorResponse } from '../../types/types';
import LectorsCreateModal from '../../components/lectors-modal-create/lectorsCreateModal';
import LectorsUpdateModal from '../../components/lectors-modal-update/lectorsUpdateModal';
import { useAppDispatch } from '../../store/hooks';
import { setLectors } from '../../store/lectors/lectorSlice';

const LectorsPage: React.FC = () => {
  const dispatch = useAppDispatch();

  const [lectors, setLectorData] = useState<ICreateLectorResponse[]>([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  console.log(lectors);

  const loadLectors = async () => {
    const response = await EntityService.getAllLectors();
    const lectorsWithoutPassword = response.map((lector) => {
      const { password, ...rest } = lector;
      return rest;
    });
    setLectorData(response);
    dispatch(setLectors(lectorsWithoutPassword));
    setDataLoaded(true);
  };

  useEffect(() => {
    if (!dataLoaded) {
      loadLectors();
    }

  }, []);

  const [lectorId, setLectorId] = useState<number>(0);
  const [lectorName, setLectorName] = useState<string | null>('');
  const [lectorSurname, setLectorSurname] = useState<string | null>('');
  const [lectorEmail, setLectorEmail] = useState<string>('');
  const [lectorPassword, setLectorPassword] = useState<string>('');
  const [visibleModalLector, setVisibleModalLector] = useState<boolean>(false);
  const [visibleUpdateModalLector, setVisibleUpdateModalLector] =
    useState<boolean>(false);

  return (
    <section className="lectors-container">
      <Header title="Lectors" />
      <div className="actions-lector">
        <button
          className="add-lector-btn"
          onClick={() => setVisibleModalLector(true)}
        >
          <img src={plusImage} alt="plus" />
          <span>Add new lector</span>
        </button>
      </div>
      {/* Header of table */}
      <div className="lector-table-header">
        <div className="lector-column-header lector-column-header-first">
          Name
        </div>
        <div className="lector-column-header">Surname</div>
        <div className="lector-column-header">Email</div>
        <div className="lector-column-header">Password</div>
        <div className="lector-column-header"></div>
      </div>
      <div className="lectors-functionality">
        {/* ModalWindow for create lector */}
        {visibleModalLector && (
          <LectorsCreateModal
            setVisibleModalLector={setVisibleModalLector}
            updateLectorsList={loadLectors}
          />
        )}
        {/* ModalWindow for update lector */}
        {visibleUpdateModalLector && (
          <LectorsUpdateModal
            id={lectorId}
            name={lectorName}
            surname={lectorSurname}
            email={lectorEmail}
            password={lectorPassword}
            setVisibleUpdateModalLector={setVisibleUpdateModalLector}
            updateLectorsList={loadLectors}
          />
        )}
        {/* Table */}
        <div className="lector-table-container">
          <div className="lector-table">
            <div className="lector-table-body">
              {lectors.map((lector) => (
                <div className="lector-row" key={lector.id}>
                  <div className="lector-column-table lector-column-table-first">
                    {lector.name}
                  </div>
                  <div className="lector-column-table">{lector.surname}</div>
                  <div className="lector-column-table">{lector.email}</div>
                  <div className="lector-column-table"></div>
                  <div className="lector-column-table lector-column-table-last">
                    <button
                      onClick={() => {
                        setLectorId(lector.id);
                        setLectorName(lector.name);
                        setLectorSurname(lector.surname);
                        setLectorEmail(lector.email);
                        setLectorPassword(lector.password);
                        setVisibleUpdateModalLector(true);
                      }}
                    ></button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default LectorsPage;
