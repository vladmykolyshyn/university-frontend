import React, { useEffect, useState } from 'react';
import './studentsPage.scss';
import Select from 'react-select';
import { useNavigate } from 'react-router-dom';
import Header from '../../components/header/header';
import plusImage from '../../img/Plus-icon.svg';
import searchIcon from '../../img/Search-icon.svg';
import { EntityService } from '../../services/entity.service';
import { IStudentResponse } from '../../types/types';
import StudentsCreateModal from '../../components/students-modal-create/studentsCreateModal';
import { useAppDispatch } from '../../store/hooks';
import { setAllStudents } from '../../store/students/studentSlice';



const StudentsPage: React.FC = () => {
  const dispatch = useAppDispatch();

  const [students, setStudents] = useState<IStudentResponse[]>([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  console.log(students);

  const loadStudents = async () => {
    const response = await EntityService.getAllStudentsWithGroupsAndCourses();
    setStudents(response);
    dispatch(setAllStudents(response));
    setDataLoaded(true);
  };

  useEffect(() => {
    if (!dataLoaded) {
      loadStudents();
    }
  }, []);

  const navigate = useNavigate();
  const [visibleModalStudent, setVisibleModalStudent] =
    useState<boolean>(false);

  const [sortField, setSortField] = useState('');
  const [sortOrder, setSortOrder] = useState('');
  const [searchValue, setSearchValue] = useState<string>('');
  const [filterType, setFilterType] = useState<string>('');
  const [actionType, setActionType] = useState<'sort' | 'filter'>('sort');

  interface Option {
    value: string;
    label: string;
  }

  const options: Option[] = [
    { value: 'name', label: 'A-Z' },
    { value: 'name', label: 'Z-A' },
    { value: 'createdAt', label: 'Newest first' },
    { value: 'createdAt', label: 'Oldest first' },
    { value: '', label: 'All' },
  ];

  const optionsSearch: Option[] = [
    { value: 'name', label: 'Name' },
    { value: 'surname', label: 'Surname' },
    { value: 'groupName', label: 'Group' },
    { value: 'courseName', label: 'Course' },
  ];

  const handleSelectChange = (selectedOption: Option | null) => {
    if (selectedOption?.value === 'name') {
      setSortField('name');
      if (selectedOption.label === 'A-Z') {
        setSortOrder('ASC');
      } else if (selectedOption.label === 'Z-A') {
        setSortOrder('DESC');
      }
      setActionType('sort');
      setFilterType('');
      setSearchValue('');
    } else if (selectedOption?.value === 'createdAt') {
      setSortField('createdAt');
      if (selectedOption.label === 'Newest first') {
        setSortOrder('ASC');
      } else if (selectedOption.label === 'Oldest first') {
        setSortOrder('DESC');
      }
      setActionType('sort');
      setFilterType('');
      setSearchValue('');
    } else {
      setSortField('');
      setSortOrder('');
    }
  };

  useEffect(() => {
    if (actionType === 'sort') {
      console.log(actionType);
      EntityService.getAllStudentsWithGroupsAndCourses(
        actionType,
        sortField,
        sortOrder,
        undefined,
        undefined,
      )
        .then((data) => {
          console.log(data);
          setStudents(data);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [actionType, sortField, sortOrder]);

  useEffect(() => {
    if (actionType === 'filter' && filterType && searchValue) {
      console.log(actionType);
      EntityService.getAllStudentsWithGroupsAndCourses(
        actionType,
        undefined,
        undefined,
        filterType,
        searchValue,
      )
        .then((data) => {
          console.log(data);
          setStudents(data);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [actionType, filterType, searchValue]);

  return (
    <section className="students-container">
      <Header title="Students" />
      <div className="actions-student">
        <span className="sort-by">Sort by</span>
        <Select
          options={options}
          defaultValue={options.find((option) => option.value === '')}
          onChange={handleSelectChange}
          isSearchable={false}
          className="custom-select"
        />
        <span className="sort-by">Search by</span>
        <Select
          options={optionsSearch}
          value={optionsSearch.find((option) => option.value === filterType)}
          onChange={(selectedOption) => {
            if (selectedOption) {
              setFilterType(selectedOption.value);
              setActionType('filter');
            }
          }}
          className="custom-select"
          isSearchable={false}
        />
        <div className="search-container">
          <img className="search-icon" src={searchIcon} alt="search" />
          <input
            type="text"
            className="search"
            value={searchValue}
            onChange={(e) => {
              setSearchValue(e.target.value);
              setActionType('filter');
            }}
            placeholder="Search"
          />
        </div>
        <button
          className="add-student-btn"
          onClick={() => setVisibleModalStudent(true)}
        >
          <img src={plusImage} alt="plus" />
          <span>Add new student</span>
        </button>
      </div>
      {/* Header of table */}
      <div className="student-table-header">
        <div className="student-column-header student-column-header-first"></div>
        <div className="student-column-header">Name</div>
        <div className="student-column-header">Surname</div>
        <div className="student-column-header">Email</div>
        <div className="student-column-header">Age</div>
        <div className="student-column-header">Course</div>
        <div className="student-column-header">Group</div>
        <div className="student-column-header"></div>
      </div>
      <div className="students-functionality">
        {/* ModalWindow for create student */}
        {visibleModalStudent && (
          <StudentsCreateModal
            setVisibleModalStudent={setVisibleModalStudent}
            updateStudentsList={loadStudents}
          />
        )}
        {/* Table */}
        <div className="student-table-container">
          <div className="student-table">
            <div className="student-table-body">
              {students.map((student) => (
                <div className="student-row" key={student.id}>
                  <div className="student-column-table student-column-table-first">
                    {student.imagePath !== null ? (
                      <img
                        className="img-avatar"
                        src={student.imagePath}
                        alt="img"
                      />
                    ) : null}
                  </div>
                  <div className="student-column-table">{student.name}</div>
                  <div className="student-column-table">{student.surname}</div>
                  <div className="student-column-table">{student.email}</div>
                  <div className="student-column-table">{student.age}</div>
                  <div className="student-column-table">
                    {student.courses && student.courses.length > 0 ? (
                      <Select
                        options={student.courses.map((course) => ({
                          value: course.id,
                          label: course.name,
                        }))}
                        value={
                          student.courses.length > 0
                            ? {
                                value: student.courses[0].id,
                                label: student.courses[0].name,
                              }
                            : null
                        }
                      />
                    ) : null}
                  </div>
                  <div className="student-column-table">
                    {student.group ? student.group.name : null}
                  </div>
                  <div className="student-column-table student-column-table-last">
                    <button
                      onClick={() => {
                        navigate(`${student.id}`, {
                          state: {
                            id: student.id,
                            name: student.name,
                            surname: student.surname,
                            email: student.email,
                            age: student.age,
                            imagePath: student.imagePath,
                          },
                        });
                      }}
                    ></button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default StudentsPage;
