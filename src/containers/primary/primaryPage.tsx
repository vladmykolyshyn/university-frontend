import React from "react";
import "./primaryPage.scss";
import { Outlet } from 'react-router-dom';
import Sidebar from "../../components/sidebar/sidebar";

const PrimaryPage: React.FC = () => {
  return (
    <div className="primary-container">
      <div className="aside-container">
        <Sidebar />
      </div>
      <div className="main-container">
        <Outlet />
      </div>
    </div>
  );
};

export default PrimaryPage;
