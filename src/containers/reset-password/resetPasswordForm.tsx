import React, { useState } from 'react';
import './resetPasswordForm.scss';
import { SubmitHandler, useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import Title from '../../components/title/title';
import Input from '../../components/input/input';
import Checkbox from '../../components/checkbox/checkbox';
import Button from '../../components/button/button';
import { Inputs } from '../sign-in/signInForm';
import { AuthService } from '../../services/auth.service';

const ResetPasswordPage: React.FC = () => {
  const headerText = 'Reset Your Password';
  const headerTextSuccess = 'Password Changed';
  const placeholderPassword = 'password';
  const aboveInputPassword = 'New Password';
  const confirmInputPassword = 'Confirm Password';
  const checkboxText = 'Show Password';
  const buttonText = 'Reset';
  const buttonLogin = 'btn-login';
  const buttonLoginText = 'Log In';

  const [showPassword, setShowPassword] = useState(false);
  const [showSuccessPage, setShowSuccessPage] = useState(true);
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<Inputs>();

  const urlParams = new URLSearchParams(window.location.search);
  const tokenFromUrl = urlParams.get('token');

  const handleCheckboxChange = () => {
    setShowPassword(!showPassword);
  };
  const ChangeStateClick = () => {
    setShowSuccessPage(!showSuccessPage);
  };
  const onSubmitChange: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(tokenFromUrl);
      console.log(data);
      const { password } = data;
      const response = await AuthService.resetPassword({
        token: tokenFromUrl,
        newPassword: password,
      });
      if (response) {
        toast.success('You pasword is successfully changed');
        ChangeStateClick();
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <>
      {showSuccessPage ? (
        <section className="container-reset-password">
          <div className="reset-title-container">
            <Title headerText={headerText} />
          </div>
          <form onSubmit={handleSubmit(onSubmitChange)}>
            <Input
              type={showPassword ? 'text' : 'password'}
              placeholder={placeholderPassword}
              aboveInputText={aboveInputPassword}
              name="password"
              register={register}
              errors={errors}
              validation={{
                required: 'Password is required',
                minLength: {
                  value: 5,
                  message: 'Password must be at least 5 characters',
                },
                maxLength: {
                  value: 30,
                  message: 'Password cannot exceed 30 characters',
                },
              }}
            />
            <Input
              type={showPassword ? 'text' : 'password'}
              placeholder={placeholderPassword}
              aboveInputText={confirmInputPassword}
              name="confirmPassword"
              register={register}
              errors={errors}
              validation={{
                required: 'Confirm Password is required',
                validate: (value: string) =>
                  value === getValues('password') || 'Passwords do not match',
              }}
            />
            <Checkbox
              checkboxText={checkboxText}
              checked={showPassword}
              onChange={handleCheckboxChange}
            />
            <Button className={buttonLogin} buttonText={buttonText} />
          </form>
        </section>
      ) : (
        <section className="container-changed-password">
          <div className="changed-title-container">
            <Title headerText={headerTextSuccess} />
          </div>
          <p className="info-change">
            You can use your new password to log into your account
          </p>
          <form>
            <div className="container-button-change">
              <Link to={'/'}>
                <Button className={buttonLogin} buttonText={buttonLoginText} />
              </Link>
            </div>
          </form>
        </section>
      )}
    </>
  );
};

export default ResetPasswordPage;
