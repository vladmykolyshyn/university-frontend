import React, { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import "./signInForm.scss";
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Input from "../../components/input/input";
import Checkbox from "../../components/checkbox/checkbox";
import Title from "../../components/title/title";
import Button from "../../components/button/button";
import { AuthService } from "../../services/auth.service";
import { useAppDispatch } from "../../store/hooks";
import { setTokenToLocalStorage } from "../../helpers/localstorage.helper";
import { login } from "../../store/user/userSlice";

export type Inputs = {
  name: string;
  surname: string;
  username: string;
  password: string;
  confirmPassword: string;
  description: string;
  hours: number;
  age: number;
  imagePath: string;
};

const LoginPage: React.FC = () => {
  const headerText = "Welcome!";
  const aboveInputEmail = "Email";
  const aboveInputPassword = "Password";
  const typeEmail = "text";
  const checkboxText = "Show Password";
  const placeholderEmail = "name@mail.com";
  const placeholderPassword = "password";
  const buttonText = "Login";
  const buttonLogin = "btn-login";
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const loginHandler: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { username, password } = data; 
      const response = await AuthService.login({ username, password });
      if (response) {
        setTokenToLocalStorage("accessToken", response.accessToken);

        const { userId, username } = response;
        const newResponse = {
          userId,
          username,
        };
        
        dispatch(login(newResponse));
        toast.success("You logged in");
        navigate("/main");
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  const handleCheckboxChange = () => {
    setShowPassword(!showPassword);
  };

  return (
    <section className="container-login">
      <div className="login-title-container">
        <Title headerText={headerText} />
      </div>
      <form onSubmit={handleSubmit(loginHandler)}>
        <Input
          type={typeEmail}
          placeholder={placeholderEmail}
          aboveInputText={aboveInputEmail}
          name="username"
          register={register}
          errors={errors}
          validation={{
            required: 'Email is required',
            minLength: {
              value: 5,
              message: 'Email must be at least 5 characters',
            },
            maxLength: {
              value: 30,
              message: 'Email cannot exceed 30 characters',
            },
            pattern: {
              value: /@/,
              message: "Email must contain '@'",
            },
          }}
        />
        <Input
          type={showPassword ? 'text' : 'password'}
          placeholder={placeholderPassword}
          aboveInputText={aboveInputPassword}
          name="password"
          register={register}
          errors={errors}
          validation={{
            required: 'Password is required',
            minLength: {
              value: 5,
              message: 'Password must be at least 5 characters',
            },
            maxLength: {
              value: 30,
              message: 'Password cannot exceed 30 characters',
            },
          }}
        />
        <Checkbox
          checkboxText={checkboxText}
          checked={showPassword}
          onChange={handleCheckboxChange}
        />
        <Button className={buttonLogin} buttonText={buttonText} />
      </form>
      <Link to={'/forgot-password'} className="password-forgot">
        <p>Forgot your password?</p>
      </Link>
      <Link to={'/sign-up'} className="account-not">
        <p>Don&apos;t have an account?</p>
      </Link>
    </section>
  );
};

export default LoginPage;
