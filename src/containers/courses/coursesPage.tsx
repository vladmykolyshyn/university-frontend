import React, { useEffect, useState } from 'react';
import './coursesPage.scss';
import Header from '../../components/header/header';
import plusImage from '../../img/Plus-icon.svg';
import { EntityService } from '../../services/entity.service';
import { ICourseResponse } from '../../types/types';
import CoursesCreateModal from '../../components/courses-modal-create/coursesCreateModal';
import CoursesUpdateModal from '../../components/courses-modal-update/coursesUpdateModal';
import { useAppDispatch } from '../../store/hooks';
import { setCourses } from '../../store/courses/courseSlice';

export const courseLoader = async () => {
  const response = await EntityService.getAllCoursesWithStudents();
  return response;
};

const CoursesPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const [courses, setCourseData] = useState<ICourseResponse[]>([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  console.log(courses);

  const loadCourse = async () => {
    const response = await EntityService.getAllCoursesWithStudents();
    setCourseData(response);
    dispatch(setCourses(response));
    setDataLoaded(true);
  };

  useEffect(() => {
    if (!dataLoaded) {
      loadCourse();
    }
  }, []);

  const [courseId, setCourseId] = useState<number>(0);
  const [courseName, setCourseName] = useState<string | null>('');
  const [courseDescription, setCourseDescription] = useState<string | null>('');
  const [courseHours, setCourseHours] = useState<number>(0);
  const [visibleModalCourse, setVisibleModalCourse] = useState<boolean>(false);
  const [visibleUpdateModalCourse, setVisibleUpdateModalCourse] =
    useState<boolean>(false);

  return (
    <section className="courses-container">
      <Header title="Courses" />
      <div className="actions-course">
        <button
          className="add-course-btn"
          onClick={() => setVisibleModalCourse(true)}
        >
          <img src={plusImage} alt="plus" />
          <span>Add new course</span>
        </button>
      </div>
      {/* Header of table */}
      <div className="course-table-header">
        <div className="course-column-header course-column-header-first">
          Name
        </div>
        <div className="course-column-header">Description</div>
        <div className="course-column-header">Hours</div>
        <div className="course-column-header">Students_count</div>
        <div className="course-column-header"></div>
      </div>

      <div className="courses-functionality">
        {/* ModalWindow for create course */}
        {visibleModalCourse && (
          <CoursesCreateModal
            setVisibleModalCourse={setVisibleModalCourse}
            updateCoursesList={loadCourse}
          />
        )}
        {/* ModalWindow for update course */}
        {visibleUpdateModalCourse && (
          <CoursesUpdateModal
            id={courseId}
            name={courseName}
            description={courseDescription}
            hours={courseHours}
            setVisibleUpdateModalCourse={setVisibleUpdateModalCourse}
            updateCoursesList={loadCourse}
          />
        )}
        {/* Table */}
        <div className="course-table-container">
          <div className="course-table">
            <div className="course-table-body">
              {courses.map((course) => (
                <div className="course-row" key={course.id}>
                  <div className="course-column-table course-column-table-first">
                    {course.name}
                  </div>
                  <div className="course-column-table">
                    {course.description}
                  </div>
                  <div className="course-column-table">{course.hours}</div>
                  <div className="course-column-table">
                    {course.studentCount}
                  </div>
                  <div className="course-column-table course-column-table-last">
                    <button
                      onClick={() => {
                        setCourseId(course.id);
                        setCourseName(course.name);
                        setCourseDescription(course.description);
                        setCourseHours(course.hours);
                        setVisibleUpdateModalCourse(true);
                      }}
                    ></button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default CoursesPage;
