import React, { useEffect, useState } from 'react';
import './studentsDetailPage.scss';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import Select from 'react-select';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useDropzone } from 'react-dropzone';
import { toast } from 'react-toastify';
import arrowLeft from '../../img/Arrow-left-icon.svg';
import HeaderDetail from '../../components/header-id/headerId';
import Input from '../../components/input/input';
import { Inputs } from '../sign-in/signInForm';
import { EntityService } from '../../services/entity.service';
import Button from '../../components/button/button';
import Circle from './../../img/Dropzone.svg';
import { ICourseResponse, IGroupResponse } from '../../types/types';
import { groupLoader } from '../groups/groupsPage';
import { courseLoader } from '../courses/coursesPage';

interface IStudentModal {
  id?: number;
  imagePath?: string | null;
  name?: string | null;
  surname?: string | null;
  email?: string | null;
  age?: number | null;
}

const StudentDetailPage: React.FC<IStudentModal> = () => {
  let params = useParams();
  console.log(params);

  const [groupData, setGroupData] = useState<IGroupResponse[]>([]);
  const [courseData, setCourseData] = useState<ICourseResponse[]>([]);
  console.log(groupData);
  console.log(courseData);

  {/* For loading data in selects with groups and courses */}
  useEffect(() => {
    const fetchGroupData = async () => {
      try {
        const groups = await groupLoader();
        setGroupData(groups);
      } catch (error) {
        console.error('Error loading group data', error);
      }
    };

    const fetchCourseData = async () => {
      try {
        const courses = await courseLoader();
        setCourseData(courses);
      } catch (error) {
        console.error('Error loading course data', error);
      }
    };

    fetchGroupData();
    fetchCourseData();
  }, []);

  const [selectedCourseId, setSelectedCourseId] = useState<number | null>(null);
  const [selectedGroupId, setSelectedGroupId] = useState<number | null>(null);

  const typeText = 'text';
  const aboveInputName = 'Name';
  const aboveInputSurname = 'Surname';
  const aboveInputEmail = 'Email';
  const aboveInputAge = 'Age';
  const placeholderName = 'John';
  const placeholderSurname = 'Smith';
  const placeholderEmail = 'name@mail.com';
  const placeholderAge = '25';
  const buttonLogin = 'btn-login';
  const buttonText = 'Save changes';
  const navigate = useNavigate();
  const location = useLocation();
  const { id, imagePath, name, surname, email, age } = location.state || {};
  const [fileBase64, setFileBase64] = useState<string | null>(
    imagePath || null,
  );

  {/* Drop zone for loading picture */}
  const onDrop = (acceptedFiles: File[]) => {
    if (acceptedFiles && acceptedFiles.length > 0) {
      const file = acceptedFiles[0];
      const reader = new FileReader();
      reader.onload = () => {
        const base64Result = reader.result as string;
        setFileBase64(base64Result);
      };
      reader.readAsDataURL(file);
    }
    console.log(fileBase64);
  };

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({
    defaultValues: {
      name: name || '',
      surname: surname || '',
      username: email || '',
      age: age || 0,
    },
  });

  console.log(selectedCourseId);
  console.log(selectedGroupId);

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name, surname, username, age } = data;
      await EntityService.updateStudent(id, {
        name: name,
        surname: surname,
        email: username,
        age: Number(age),
        imagePath: fileBase64 || undefined,
      });
      if (selectedCourseId !== null) {
        await EntityService.addStudentToCourse(Number(selectedCourseId), {
          studentId: id,
        });
      }
      if (selectedGroupId !== null) {
        await EntityService.addStudentToGroup(id, {
          groupId: Number(selectedGroupId),
        });
      }
      toast.success('Student is successfully updated');
      navigate('/main/students');
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="student-detail-container">
      <HeaderDetail />
      <div className="main-container">
        <button
          className="btn-back"
          onClick={() => {
            navigate(-1);
          }}
        >
          <img src={arrowLeft} alt="back" />
          <span>Back</span>
        </button>
        <div className="info-container">
          <div className="left-info">
            <h1>Personal information</h1>
            <div {...getRootProps()} className="dropzone">
              <input {...getInputProps()} />
              <img src={fileBase64 || Circle} alt="drop" />
              <div className="file-section">
                <button className="btn-drop">Replace</button>
                <p>
                  Must be a .jpg or .png file smaller than 10MB and at least
                  400px by 400px.
                </p>
              </div>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Input
                type={typeText}
                placeholder={placeholderName}
                aboveInputText={aboveInputName}
                name="name"
                register={register}
                errors={errors}
                validation={{
                  required: 'Name is required',
                  minLength: {
                    value: 3,
                    message: 'Name must be at least 3 characters',
                  },
                  maxLength: {
                    value: 20,
                    message: 'Name cannot exceed 20 characters',
                  },
                }}
              />
              <Input
                type={typeText}
                placeholder={placeholderSurname}
                aboveInputText={aboveInputSurname}
                name="surname"
                register={register}
                errors={errors}
                validation={{
                  required: 'Surname is required',
                  minLength: {
                    value: 3,
                    message: 'Surname must be at least 3 characters',
                  },
                  maxLength: {
                    value: 20,
                    message: 'Surname cannot exceed 20 characters',
                  },
                }}
              />
              <Input
                type={typeText}
                placeholder={placeholderEmail}
                aboveInputText={aboveInputEmail}
                name="username"
                register={register}
                errors={errors}
                validation={{
                  required: 'Email is required',
                  minLength: {
                    value: 5,
                    message: 'Email must be at least 5 characters',
                  },
                  maxLength: {
                    value: 30,
                    message: 'Email cannot exceed 30 characters',
                  },
                  pattern: {
                    value: /@/,
                    message: "Email must contain '@'",
                  },
                }}
              />
              <Input
                type={typeText}
                placeholder={placeholderAge}
                aboveInputText={aboveInputAge}
                name="age"
                register={register}
                errors={errors}
                validation={{
                  required: 'Age is required',
                  minLength: {
                    value: 1,
                    message: 'Age must be at least 1 characters',
                  },
                  maxLength: {
                    value: 3,
                    message: 'Age cannot exceed 3 characters',
                  },
                }}
              />
              <div className="student-container-button-update">
                <Button className={buttonLogin} buttonText={buttonText} />
              </div>
            </form>
          </div>
          <div className="right-info">
            <h2>Courses and Groups</h2>
            <div>
              <p className="text-above">Course</p>
              {courseData && courseData.length > 0 ? (
                <Select
                  className="select-list"
                  isSearchable={false}
                  onChange={(selectedOption) => {
                    setSelectedCourseId(selectedOption?.value || null);
                  }}
                  options={courseData.map((course) => ({
                    value: course.id,
                    label: course.name,
                  }))}
                  styles={{
                    control: (baseStyles) => ({
                      ...baseStyles,
                      height: '48px',
                      border: '1px solid #d1d8db',
                      borderRadius: '0px',
                    }),
                    singleValue: (baseStyles) => ({
                      ...baseStyles,
                      color: '#000',
                      fontSize: '15px',
                      lineHeight: '24px',
                      fontFamily: `'Lexend', sans-serif`,
                      fontWeight: '400px',
                    }),
                  }}
                />
              ) : null}
            </div>
            <div>
              <p className="text-above">Group</p>
              {groupData && groupData.length > 0 ? (
                <Select
                  className="select-list"
                  isSearchable={false}
                  onChange={(selectedOption) => {
                    setSelectedGroupId(selectedOption?.value || null);
                  }}
                  options={groupData.map((group) => ({
                    value: group.id,
                    label: group.name,
                  }))}
                  styles={{
                    control: (baseStyles) => ({
                      ...baseStyles,
                      height: '48px',
                      border: '1px solid #d1d8db',
                      borderRadius: '0px',
                    }),
                    singleValue: (baseStyles) => ({
                      ...baseStyles,
                      color: '#000',
                      fontSize: '15px',
                      lineHeight: '24px',
                      fontFamily: `'Lexend', sans-serif`,
                      fontWeight: '400px',
                    }),
                  }}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default StudentDetailPage;
