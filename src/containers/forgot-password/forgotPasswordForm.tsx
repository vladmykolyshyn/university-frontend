import React from "react";
import "./forgotPasswordForm.scss"
import { SubmitHandler, useForm } from 'react-hook-form';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Title from "../../components/title/title";
import Input from "../../components/input/input";
import Button from "../../components/button/button";
import { Inputs } from "../sign-in/signInForm";
import { AuthService } from "../../services/auth.service";

const ForgotPasswordPage: React.FC = () => {
  const headerText = "Forgot Password";
  const typeEmail = "text";
  const placeholder = "name@mail.com";
  const aboveInputEmail = "Email";
  const buttonLogin = "btn-login";
  const buttonCancel = "btn-cancel"
  const buttonTextReset = "Reset";
  const buttonTextCancel = "Cancel";
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();
  
  const resetPasswordRequestHandler: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { username } = data;
      const response = await AuthService.resetPasswordRequest({ username });
      if (response) {
        toast.success('You request is confirmed in your mail');
        navigate('/');
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="container-forgot-password">
      <div className="forgot-title-container">
        <Title headerText={headerText} />
      </div>
      <p className="info-reset">
        Don&apos;t worry, happens to the best of us. Enter the email address
        associated with your account and we&apos;ll send you a link to reset.
      </p>
      <form onSubmit={handleSubmit(resetPasswordRequestHandler)}>
        <Input
          type={typeEmail}
          placeholder={placeholder}
          aboveInputText={aboveInputEmail}
          name="username"
          register={register}
          errors={errors}
          validation={{
            required: 'Email is required',
            minLength: {
              value: 5,
              message: 'Email must be at least 5 characters',
            },
            maxLength: {
              value: 30,
              message: 'Email cannot exceed 30 characters',
            },
            pattern: {
              value: /@/,
              message: "Email must contain '@'",
            },
          }}
        />
        <div className="container-button-reset">
          <Button className={buttonLogin} buttonText={buttonTextReset} />
          <Link to={'/'}>
            <Button className={buttonCancel} buttonText={buttonTextCancel} />
          </Link>
        </div>
      </form>
    </section>
  );
};

export default ForgotPasswordPage;
