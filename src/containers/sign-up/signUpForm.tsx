import React, { useState } from "react";
import "./signUpForm.scss";
import { SubmitHandler, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { Link, useNavigate } from 'react-router-dom';
import Title from "../../components/title/title";
import Input from "../../components/input/input";
import Checkbox from "../../components/checkbox/checkbox";
import Button from "../../components/button/button";
import { Inputs } from "../sign-in/signInForm";
import { AuthService } from "../../services/auth.service";

const RegistrationPage: React.FC = () => {
  const headerText = "Register your account";
  const typeEmail = "text";
  const placeholderEmail = "name@mail.com";
  const placeholderPassword = "password";
  const aboveInputEmail = "Email";
  const aboveInputPassword = "Password";
  const confirmInputPassword = "Confirm Password";
  const checkboxText = "Show Password";
  const buttonText = "Register";
  const buttonLogin = "btn-login";
  const navigate = useNavigate();

  const [showPassword, setShowPassword] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<Inputs>();

  const registrationHandler: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { username, password } = data; 
      const response = await AuthService.registration({ username, password });
      if (response) {
        toast.success("Account has been created");
        navigate("/");
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  const handleCheckboxChange = () => {
    setShowPassword(!showPassword);
  };

  return (
    <section className="container-registration">
      <div className="reg-title-container">
        <Title headerText={headerText} />
      </div>
      <form onSubmit={handleSubmit(registrationHandler)}>
        <Input
          type={typeEmail}
          placeholder={placeholderEmail}
          aboveInputText={aboveInputEmail}
          name="username"
          register={register}
          errors={errors}
          validation={{
            required: "Email is required",
            minLength: {
              value: 5,
              message: "Email must be at least 5 characters",
            },
            maxLength: {
              value: 30,
              message: "Email cannot exceed 30 characters",
            },
            pattern: {
              value: /@/,
              message: "Email must contain '@'",
            },
          }}
        />
        <Input
          type={showPassword ? "text" : "password"}
          placeholder={placeholderPassword}
          aboveInputText={aboveInputPassword}
          name="password"
          register={register}
          errors={errors}
          validation={{
            required: "Password is required",
            minLength: {
              value: 5,
              message: "Password must be at least 5 characters",
            },
            maxLength: {
              value: 30,
              message: "Password cannot exceed 30 characters",
            },
          }}
        />
        <Input
          type={showPassword ? "text" : "password"}
          placeholder={placeholderPassword}
          aboveInputText={confirmInputPassword}
          name="confirmPassword"
          register={register}
          errors={errors}
          validation={{
            required: "Confirm Password is required",
            validate: (value: string) =>
              value === getValues("password") || "Passwords do not match",
          }}
        />
        <Checkbox
          checkboxText={checkboxText}
          checked={showPassword}
          onChange={handleCheckboxChange}
        />
        <Button className={buttonLogin} buttonText={buttonText} />
      </form>
      <Link to={"/"} className="account-is">
        <p>Have an account?</p>
      </Link>
    </section>
  );
};

export default RegistrationPage;
