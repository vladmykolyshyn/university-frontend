import React from 'react';
import { createBrowserRouter } from 'react-router-dom';
import LoginPage from '../containers/sign-in/signInForm';
import RegistrationPage from '../containers/sign-up/signUpForm';
import ResetPasswordPage from '../containers/reset-password/resetPasswordForm';
import ForgotPasswordPage from '../containers/forgot-password/forgotPasswordForm';
import PrimaryPage from '../containers/primary/primaryPage';
import CoursesPage from '../containers/courses/coursesPage';
import LectorsPage from '../containers/lectors/lectorsPage';
import GroupsPage from '../containers/groups/groupsPage';
import StudentsPage from '../containers/students/studentsPage';
import StudentDetailPage from '../containers/student-detail/studentsDetailPage';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <LoginPage />,
  },
  {
    path: '/sign-up',
    element: <RegistrationPage />,
  },
  {
    path: '/reset-password',
    element: <ResetPasswordPage />,
  },
  {
    path: '/forgot-password',
    element: <ForgotPasswordPage />,
  },
  {
    path: '/main',
    element: <PrimaryPage />,
    children: [
      {
        path: 'courses',
        element: <CoursesPage />,
      },
      {
        path: 'lectors',
        element: <LectorsPage />,
      },
      {
        path: 'groups',
        element: <GroupsPage />,
      },
      {
        path: 'students',
        element: <StudentsPage />,
      },
      {},
    ],
  },
  { path: 'main/students/:id', element: <StudentDetailPage /> },
]);
