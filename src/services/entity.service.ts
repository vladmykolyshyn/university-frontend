import { instance } from '../api/axios.api';
import {
  IAddStudentToCourse,
  IAddStudentToGroup,
  ICourseResponse,
  ICreateCourse,
  ICreateCourseResponse,
  ICreateGroup,
  ICreateGroupResponse,
  ICreateLector,
  ICreateLectorResponse,
  ICreateStudent,
  ICreateStudentResponse,
  IGroupResponse,
  IStudentResponse,
  IUpdateCourse,
  IUpdateGroup,
  IUpdateLector,
  IUpdateStudent,
} from '../types/types';

export const EntityService = {
  async createLector(
    userData: ICreateLector,
  ): Promise<ICreateLectorResponse | undefined> {
    const { data } = await instance.post<ICreateLectorResponse>(
      'lectors',
      userData,
    );
    return data;
  },

  async createCourse(
    userData: ICreateCourse,
  ): Promise<ICreateCourseResponse | undefined> {
    const { data } = await instance.post<ICreateCourseResponse>(
      'courses',
      userData,
    );
    return data;
  },

  async createGroup(
    userData: ICreateGroup,
  ): Promise<ICreateGroupResponse | undefined> {
    const { data } = await instance.post<ICreateGroupResponse>(
      'groups',
      userData,
    );
    return data;
  },

  async createStudent(
    userData: ICreateStudent,
  ): Promise<ICreateStudentResponse | undefined> {
    const { data } = await instance.post<ICreateStudentResponse>(
      'students',
      userData,
    );
    return data;
  },

  async updateLector(
    idData: number | undefined,
    userData: IUpdateLector,
  ): Promise<void | undefined> {
    const { data } = await instance.patch(`lectors/${idData}`, userData);
    return data;
  },

  async updateCourse(
    idData: number | undefined,
    userData: IUpdateCourse,
  ): Promise<void | undefined> {
    const { data } = await instance.patch(`courses/${idData}`, userData);
    return data;
  },

  async updateGroup(
    idData: number | undefined,
    userData: IUpdateGroup,
  ): Promise<void | undefined> {
    const { data } = await instance.patch(`groups/${idData}`, userData);
    return data;
  },

  async updateStudent(
    idData: number | undefined,
    userData: IUpdateStudent,
  ): Promise<void | undefined> {
    const { data } = await instance.patch(`students/${idData}`, userData);
    return data;
  },

  async addStudentToCourse(
    idData: number | undefined,
    userData: IAddStudentToCourse,
  ): Promise<void | undefined> {
    const { data } = await instance.patch(`courses/${idData}/student`, userData);
    return data;
  },

  async addStudentToGroup(
    idData: number | undefined,
    userData: IAddStudentToGroup,
  ): Promise<void | undefined> {
    const { data } = await instance.patch(`students/${idData}/group`, userData);
    return data;
  },

  async getAllLectors(): Promise<ICreateLectorResponse[]> {
    const { data } = await instance.get<ICreateLectorResponse[]>('lectors');
    return data;
  },

  async getAllCoursesWithStudents(): Promise<ICourseResponse[]> {
    const { data } = await instance.get<ICourseResponse[]>('courses/students');
    return data;
  },

  async getAllGroups(): Promise<IGroupResponse[]> {
    const { data } = await instance.get<IGroupResponse[]>('groups');
    return data;
  },

  async getAllStudentsWithGroupsAndCourses(
    actionType?: string,
    sortField?: string,
    sortOrder?: string,
    filterType?: string,
    searchValue?: string,
  ): Promise<IStudentResponse[]> {
    let queryParams = '';

    if (actionType === 'sort') {
      queryParams = `?sortField=${sortField}&sortOrder=${sortOrder}`;
    }

    if (actionType === 'filter') {
      queryParams = `?${filterType}=${searchValue}`;
    }

    const url = queryParams
      ? `students/groups${queryParams}`
      : 'students/groups';

    console.log(url);
    const { data } = await instance.get<IStudentResponse[]>(url);
    return data;
  },
};
