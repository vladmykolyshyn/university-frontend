import { configureStore} from "@reduxjs/toolkit";
import userReducer from "./user/userSlice";
import lectorsReducer from "./lectors/lectorSlice"
import coursesReducer from "./courses/courseSlice"
import groupsReducer from "./groups/groupSlice"
import studentsReducer from "./students/studentSlice"


export const store = configureStore({
  reducer: {
    user: userReducer,
    lectors: lectorsReducer,
    courses: coursesReducer,
    groups: groupsReducer,
    students: studentsReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
