import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../store";
import {IGroupResponse} from "../../types/types";

// Define a type for the slice state
interface GroupsState {
  groups: IGroupResponse[] | null;
}

// Define the initial state using that type
const initialState: GroupsState = {
  groups: [],
};

export const groupsSlice = createSlice({
  name: 'groups',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setGroups: (state, action: PayloadAction<IGroupResponse[]>) => {
      state.groups = action.payload;
    },
    clearGroups: (state) => {
      state.groups = null;
    },
  },
});

export const { setGroups, clearGroups } = groupsSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.groups;


export default groupsSlice.reducer;
