import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../store";
import {IStudentResponse} from "../../types/types";

// Define a type for the slice state
interface StudentsState {
  students: IStudentResponse[] | null;
}

// Define the initial state using that type
const initialState: StudentsState = {
  students: [],
};

export const studentsSlice = createSlice({
  name: 'students',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setAllStudents: (state, action: PayloadAction<IStudentResponse[]>) => {
      state.students = action.payload;
    },
    clearStudents: (state) => {
      state.students = null;
    },
  },
});

export const { setAllStudents, clearStudents } = studentsSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.students;


export default studentsSlice.reducer;
