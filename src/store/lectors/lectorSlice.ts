import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../store";
import {ILectorResponse} from "../../types/types";

// Define a type for the slice state
interface LectorsState {
  lectors: ILectorResponse[] | null;
}

// Define the initial state using that type
const initialState: LectorsState = {
  lectors: [],
};

export const lectorsSlice = createSlice({
  name: 'lectors',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setLectors: (state, action: PayloadAction<ILectorResponse[]>) => {
      state.lectors = action.payload;
    },
    clearLectors: (state) => {
      state.lectors = null;
    },
  },
});

export const { setLectors, clearLectors } = lectorsSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.lectors;


export default lectorsSlice.reducer;
