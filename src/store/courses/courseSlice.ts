import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../store";
import {ICourseResponse} from "../../types/types";

// Define a type for the slice state
interface CoursesState {
  courses: ICourseResponse[] | null;
}

// Define the initial state using that type
const initialState: CoursesState = {
  courses: [],
};

export const coursesSlice = createSlice({
  name: 'courses',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    setCourses: (state, action: PayloadAction<ICourseResponse[]>) => {
      state.courses = action.payload;
    },
    clearCourses: (state) => {
      state.courses = null;
    },
  },
});

export const { setCourses, clearCourses } = coursesSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.courses;


export default coursesSlice.reducer;
