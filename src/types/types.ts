export interface IUser {
  userId: number;
  username: string;
  accessToken: string;
}

export interface IUserLogin {
  userId: number;
  username: string;
}

export interface IResetPasswordRequest {
  username: string;
}

export interface IResetPasswordRequestResponse {
  userId: string;
  token: string;
}

export interface IResetPassword {
  token: string | null;
  newPassword: string;
}

export interface IUserId {
  userId: number;
}

export interface IUserData {
  username: string;
  password: string;
}

export interface IResponseUserData {
  accessToken: string;
  userId: number;
  username: string;
  password: string;
}

export interface ILectorResponse {
  name: string | null;
  surname: string | null;
  email: string;
  id: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface ICreateLector {
  name?: string;
  surname?: string;
  email: string;
  password: string;
}

export interface ICreateCourse {
  name?: string;
  description: string;
  hours: number;
}

export interface ICreateGroup {
  name?: string;
}

export interface ICreateStudent {
  name: string;
  surname: string;
  email: string;
  age: number;
}

export interface IAddStudentToGroup {
  groupId: number;
}

export interface IAddStudentToCourse {
  studentId: number;
}

export interface IUpdateStudent {
  name?: string;
  surname?: string;
  email?: string;
  age?: number;
  imagePath?: string;
}

export interface IUpdateLector {
  name?: string;
  surname?: string;
  email?: string;
  password?: string;
}

export interface IUpdateCourse {
  name?: string;
  description?: string;
  hours?: number;
}

export interface IUpdateGroup {
  name?: string;
}

export interface ICreateLectorResponse {
  name: string | null;
  surname: string | null;
  email: string;
  password: string;
  id: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface ICreateCourseResponse {
  name: string;
  description: string;
  hours: number;
  id: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface ICreateGroupResponse {
  name: string;
  id: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface ICreateStudentResponse {
  name: string;
  surname: string;
  age: number;
  email: string;
  imagepath: string;
  groupId: null;
  id: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface ICourseResponse {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  description: string;
  hours: number;
  students: string[];
  studentCount: number;
}

export interface IGroupResponse {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  students: string[];
}

export interface ICourseStudent {
  id: number;
  name: string;
}

export interface IStudentResponse {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath: string;
  groupId: number;
  group: IGroupResponse;
  courses: ICourseStudent[];
}
