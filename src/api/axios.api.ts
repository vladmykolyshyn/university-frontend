//http://localhost:3001/api/v1/auth/sign-in

import axios from "axios";
import { getTokenFromLocalStorage } from "../helpers/localstorage.helper";

export const instance = axios.create({
  baseURL: "http://localhost:3001/api/v1",
  headers: {
    Authorization: "Bearer " + getTokenFromLocalStorage() || "",
  },
});
