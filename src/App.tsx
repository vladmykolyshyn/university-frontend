import React, { useEffect } from "react";
import "./styles/main.scss";
import { useAppDispatch } from "./store/hooks";
import { getTokenFromLocalStorage } from "./helpers/localstorage.helper";
import { AuthService } from "./services/auth.service";
import { login, logout } from "./store/user/userSlice";
import { router } from "./router/router";
import { RouterProvider } from "react-router-dom";

function App() {
  
  const dispatch = useAppDispatch();

  const checkAuth = async () => {
    const accessToken = getTokenFromLocalStorage();
    try {
      if (accessToken) {
        const data = await AuthService.getProfile();

        if (data) {
          dispatch(login(data))
        } else {
          dispatch(logout())
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    checkAuth()
  }, [])

  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
