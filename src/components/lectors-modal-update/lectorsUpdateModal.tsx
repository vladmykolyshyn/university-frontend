import React from 'react';
import './lectorsUpdateModal.scss';
import { SubmitHandler, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import Input from '../input/input';
import { Inputs } from '../../containers/sign-in/signInForm';
import Button from '../button/button';
import closeModalImage from '../../img/CloseModal-icon.svg';
import { EntityService } from '../../services/entity.service';

interface ILectorModal {
  id?: number;
  name?: string | null;
  surname?: string | null;
  email?: string | null;
  password?: string | null;
  setVisibleUpdateModalLector: (visible: boolean) => void;
  updateLectorsList: () => void;
}

const LectorsUpdateModal: React.FC<ILectorModal> = ({
  id,
  name,
  surname,
  email,
  password,
  setVisibleUpdateModalLector,
  updateLectorsList,
}) => {
  const typeText = 'text';
  const aboveInputName = 'Name';
  const aboveInputSurname = 'Surname';
  const aboveInputEmail = 'Email';
  const aboveInputPassword = 'Password';
  const placeholderName = 'John';
  const placeholderSurname = 'Smith';
  const placeholderEmail = 'name@mail.com';
  const placeholderPassword = 'password';
  const buttonLogin = 'btn-login';
  const buttonText = 'Save';
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({
    defaultValues: {
      name: name || '',
      surname: surname || '',
      username: email || '',
      password: password || '',
    },
  });

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name, surname, username, password } = data;
      await EntityService.updateLector(id, {
        name: name,
        surname: surname,
        email: username,
        password: password,
      });
      toast.success('Lector is successfully updated');
      setVisibleUpdateModalLector(false);
      updateLectorsList();
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="lector-modal-container">
      <main className="modal-main-info">
        <button
          className="close-modal-btn"
          onClick={() => setVisibleUpdateModalLector(false)}
        >
          <img src={closeModalImage} alt="close" />
        </button>
        <h1>Edit lector</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            type={typeText}
            placeholder={placeholderName}
            aboveInputText={aboveInputName}
            name="name"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 3,
                message: 'Name must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Name cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderSurname}
            aboveInputText={aboveInputSurname}
            name="surname"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 3,
                message: 'Surname must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Surname cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderEmail}
            aboveInputText={aboveInputEmail}
            name="username"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 5,
                message: 'Email must be at least 5 characters',
              },
              maxLength: {
                value: 30,
                message: 'Email cannot exceed 30 characters',
              },
              pattern: {
                value: /@/,
                message: "Email must contain '@'",
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderPassword}
            aboveInputText={aboveInputPassword}
            name="password"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 5,
                message: 'Password must be at least 5 characters',
              },
              maxLength: {
                value: 100,
                message: 'Password cannot exceed 30 characters',
              },
            }}
          />
          <div className="container-button-create">
            <Button className={buttonLogin} buttonText={buttonText} />
          </div>
        </form>
      </main>
    </section>
  );
};

export default LectorsUpdateModal;
