import React from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import './groupsCreateModal.scss';
import Input from '../input/input';
import { Inputs } from '../../containers/sign-in/signInForm';
import Button from '../button/button';
import closeModalImage from '../../img/CloseModal-icon.svg';
import { EntityService } from '../../services/entity.service';

interface IGroupModal {
  setVisibleModalGroup: (visible: boolean) => void;
  updateGroupsList: () => void;
}

const GroupsCreateModal: React.FC<IGroupModal> = ({
  setVisibleModalGroup,
  updateGroupsList,
}) => {
  const typeText = 'text';
  const aboveInputName = 'Name';
  const placeholderName = 'John';
  const buttonLogin = 'btn-login';
  const buttonText = 'Create';

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name } = data;
      const response = await EntityService.createGroup({
        name: name,
      });
      if (response) {
        toast.success('Group is successfully created');
        setVisibleModalGroup(false);
        updateGroupsList();
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="group-modal-container">
      <main className="group-modal-main-info">
        <button
          className="group-close-modal-btn"
          onClick={() => setVisibleModalGroup(false)}
        >
          <img src={closeModalImage} alt="close" />
        </button>
        <h1>Add new group</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            type={typeText}
            placeholder={placeholderName}
            aboveInputText={aboveInputName}
            name="name"
            register={register}
            errors={errors}
            validation={{
              required: 'Name is required',
              minLength: {
                value: 3,
                message: 'Name must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Name cannot exceed 20 characters',
              },
            }}
          />
          <div className="group-container-button-create">
            <Button className={buttonLogin} buttonText={buttonText} />
          </div>
        </form>
      </main>
    </section>
  );
};

export default GroupsCreateModal;
