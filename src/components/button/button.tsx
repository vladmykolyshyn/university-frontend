import React from "react";
import "./button.scss"

interface SubmitButtonProps {
  buttonText: string;
  className: string;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const Button: React.FC<SubmitButtonProps> = ({ buttonText, onClick, className }) => {
  return (
    <>
      <button
        className={`${className}`}
        type="submit"
        onClick={onClick}
      >
        <p>{buttonText}</p>
      </button>
    </>
  );
};

export default Button;
