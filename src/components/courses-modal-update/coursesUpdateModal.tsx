import React from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import './coursesUpdateModal.scss';
import Input from '../input/input';
import { Inputs } from '../../containers/sign-in/signInForm';
import Button from '../button/button';
import closeModalImage from '../../img/CloseModal-icon.svg';
import { EntityService } from '../../services/entity.service';

interface ICourseModal {
  id?: number;
  name?: string | null;
  description?: string | null;
  hours?: number | null;
  setVisibleUpdateModalCourse: (visible: boolean) => void;
  updateCoursesList: () => void;
}

const CoursesUpdateModal: React.FC<ICourseModal> = ({
  id,
  name,
  description,
  hours,
  setVisibleUpdateModalCourse,
  updateCoursesList,
}) => {
  const typeText = 'text';
  const aboveInputName = 'Name';
  const aboveInputDescription = 'Description';
  const aboveInputHours = 'Hours';
  const placeholderName = 'John';
  const placeholderDescription = 'Interesting';
  const placeholderHours = '95';
  const buttonLogin = 'btn-login';
  const buttonText = 'Save';
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({
    defaultValues: {
      name: name || '',
      description: description || '',
      hours: hours || 0,
    },
  });

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name, description, hours } = data;
      await EntityService.updateCourse(id, {
        name: name,
        description: description,
        hours: Number(hours),
      });
      toast.success('Course is successfully updated');
      setVisibleUpdateModalCourse(false);
      updateCoursesList();
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="course-modal-container">
      <main className="course-modal-main-info">
        <button
          className="course-close-modal-btn"
          onClick={() => setVisibleUpdateModalCourse(false)}
        >
          <img src={closeModalImage} alt="close" />
        </button>
        <h1>Edit course</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            type={typeText}
            placeholder={placeholderName}
            aboveInputText={aboveInputName}
            name="name"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 5,
                message: 'Name must be at least 5 characters',
              },
              maxLength: {
                value: 20,
                message: 'Name cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderDescription}
            aboveInputText={aboveInputDescription}
            name="description"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 5,
                message: 'Description must be at least 5 characters',
              },
              maxLength: {
                value: 20,
                message: 'Description cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderHours}
            aboveInputText={aboveInputHours}
            name="hours"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 1,
                message: 'Hours must be at least 1 characters',
              },
              maxLength: {
                value: 30,
                message: 'Email cannot exceed 30 characters',
              },
            }}
          />
          <div className="course-container-button-create">
            <Button className={buttonLogin} buttonText={buttonText} />
          </div>
        </form>
      </main>
    </section>
  );
};

export default CoursesUpdateModal;
