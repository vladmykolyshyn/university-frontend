import React from "react";
import "./checkbox.scss"

interface CheckboxProps {
  checkboxText: string;
  checked: boolean;
  onChange: () => void;
}

const Checkbox: React.FC<CheckboxProps> = ({ checkboxText, checked, onChange }) => {

  const checkboxId = "checkbox-" + checkboxText.toLowerCase().replace(/\s/g, "-");

  return (
    <div className="checkbox-container">
      <input className="checkbox-field"
        id={checkboxId}
        type="checkbox"
        checked={checked}
        onChange={onChange}
      />
      <label className="checkbox-label" htmlFor={checkboxId}>
        <p className="checkbox-text">{checkboxText}</p>
      </label>
    </div>
  );
};

export default Checkbox;
