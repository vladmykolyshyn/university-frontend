import React from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import './groupsUpdateModal.scss';
import { toast } from 'react-toastify';
import Input from '../input/input';
import { Inputs } from '../../containers/sign-in/signInForm';
import Button from '../button/button';
import closeModalImage from '../../img/CloseModal-icon.svg';
import { EntityService } from '../../services/entity.service';

interface IGroupModal {
  id?: number;
  name?: string | null;
  setVisibleUpdateModalGroup: (visible: boolean) => void;
  updateGroupsList: () => void;
}

const GroupsUpdateModal: React.FC<IGroupModal> = ({
  id,
  name,
  setVisibleUpdateModalGroup,
  updateGroupsList,
}) => {
  const typeText = 'text';
  const aboveInputName = 'Name';
  const placeholderName = 'John';
  const buttonLogin = 'btn-login';
  const buttonText = 'Save';
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({
    defaultValues: {
      name: name || '',
    },
  });

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name } = data;
      await EntityService.updateGroup(id, {
        name: name,
      });
      toast.success('Group is successfully updated');
      setVisibleUpdateModalGroup(false);
      updateGroupsList();
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="group-modal-container">
      <main className="modal-main-info">
        <button
          className="close-modal-btn"
          onClick={() => setVisibleUpdateModalGroup(false)}
        >
          <img src={closeModalImage} alt="close" />
        </button>
        <h1>Edit group</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            type={typeText}
            placeholder={placeholderName}
            aboveInputText={aboveInputName}
            name="name"
            register={register}
            errors={errors}
            validation={{
              minLength: {
                value: 3,
                message: 'Name must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Name cannot exceed 20 characters',
              },
            }}
          />
          <div className="container-button-create">
            <Button className={buttonLogin} buttonText={buttonText} />
          </div>
        </form>
      </main>
    </section>
  );
};

export default GroupsUpdateModal;
