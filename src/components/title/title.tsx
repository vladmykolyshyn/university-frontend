import React from "react";
import "./title.scss";
import blockImage from "../../img/block.svg";

interface TitleProps {
  headerText: string;
}

const Title: React.FC<TitleProps> = ({ headerText }) => {
  return (
    <>
      <img src={blockImage} alt="block" />
      <h1>{headerText}</h1>
    </>
  );
};

export default Title;
