import React from "react";
import "./header.scss";
import avatar from "../../img/Avatar.svg";

interface HeaderProps {
  title: string;
}

const Header: React.FC<HeaderProps> = ({ title }) => {
  return (
    <header className="header-container">
      <h1>{title}</h1>
      <img src={avatar} alt="avatar" />
    </header>
  );
};

export default Header;
