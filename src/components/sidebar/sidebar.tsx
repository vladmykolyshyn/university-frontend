import React, { useEffect, useState } from 'react';
import './sidebar.scss';
import { NavLink, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import dashboardLogo from '../../img/Dashboard-logo.svg';
import dashboardIcon from '../../img/Dashboard-icon.svg';
import coursesLogo from '../../img/Courses-icon.svg';
import lectorsLogo from '../../img/Lectors-icon.svg';
import groupsLogo from '../../img/Groups-icon.svg';
import studentsLogo from '../../img/Students-icon.svg';
import logoutIcon from '../../img/logout.svg';
import { useAppDispatch } from '../../store/hooks';
import { logout } from '../../store/user/userSlice';
import { removeTokenFromLocalStorage } from '../../helpers/localstorage.helper';
import { clearLectors } from '../../store/lectors/lectorSlice';
import { clearCourses } from '../../store/courses/courseSlice';
import { clearGroups } from '../../store/groups/groupSlice';
import { clearStudents } from '../../store/students/studentSlice';

const Sidebar: React.FC = () => {
  const [activeNavLink, setActiveNavLink] = useState<string | null>(null);
  const handleNavLinkClick = (to: string) => {
    setActiveNavLink(to);
  };

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const logoutHandler = () => {
    dispatch(logout());
    dispatch(clearLectors());
    dispatch(clearCourses());
    dispatch(clearGroups());
    dispatch(clearStudents());
    removeTokenFromLocalStorage('accessToken');
    toast.success('You logged out');
    navigate('/');
  };

  // Leave the link active after refresh the page
  useEffect(() => {
    const currentPath = window.location.pathname;
    const pathWithoutMain = currentPath.replace('/main', '');
    setActiveNavLink(pathWithoutMain);
  }, []);

  return (
    <aside className="sidebar-container">
      <div>
        <div className="logo-container">
          <NavLink
            to={'/main'}
            onClick={() => handleNavLinkClick('/main')}
            className={activeNavLink === '/main' ? '' : ''}
          >
            <img className="dashboard-logo" src={dashboardLogo} alt="logo" />
          </NavLink>
        </div>
        <nav className="nav-container">
          <ul className="nav-list">
            <NavLink
              to={'/main'}
              onClick={() => handleNavLinkClick('/main')}
              className={activeNavLink === '/main' ? '' : ''}
            >
              <li className="top-li">
                <img
                  src={dashboardIcon}
                  alt="icon"
                  className={activeNavLink === '/main' ? '' : ''}
                />
                <p className={activeNavLink === '/main' ? '' : ''}>Dashboard</p>
              </li>
            </NavLink>
            <NavLink
              to={'courses'}
              onClick={() => handleNavLinkClick('/courses')}
              className={activeNavLink === '/courses' ? 'active-link' : ''}
            >
              <li>
                <img
                  src={coursesLogo}
                  alt="icon"
                  className={activeNavLink === '/courses' ? 'active-img' : ''}
                />
                <p
                  className={activeNavLink === '/courses' ? 'active-text' : ''}
                >
                  Courses
                </p>
              </li>
            </NavLink>
            <NavLink
              to={'lectors'}
              onClick={() => handleNavLinkClick('/lectors')}
              className={activeNavLink === '/lectors' ? 'active-link' : ''}
            >
              <li>
                <img
                  src={lectorsLogo}
                  alt="icon"
                  className={activeNavLink === '/lectors' ? 'active-img' : ''}
                />
                <p
                  className={activeNavLink === '/lectors' ? 'active-text' : ''}
                >
                  Lectors
                </p>
              </li>
            </NavLink>
            <NavLink
              to={'groups'}
              onClick={() => handleNavLinkClick('/groups')}
              className={activeNavLink === '/groups' ? 'active-link' : ''}
            >
              <li>
                <img
                  src={groupsLogo}
                  alt="icon"
                  className={activeNavLink === '/groups' ? 'active-img' : ''}
                />
                <p className={activeNavLink === '/groups' ? 'active-text' : ''}>
                  Groups
                </p>
              </li>
            </NavLink>
            <NavLink
              to={'students'}
              onClick={() => handleNavLinkClick('/students')}
              className={activeNavLink === '/students' ? 'active-link' : ''}
            >
              <li>
                <img
                  src={studentsLogo}
                  alt="icon"
                  className={activeNavLink === '/students' ? 'active-img' : ''}
                />
                <p
                  className={activeNavLink === '/students' ? 'active-text' : ''}
                >
                  Students
                </p>
              </li>
            </NavLink>
          </ul>
        </nav>
      </div>
      <button className="btn-logout" onClick={logoutHandler}>
        <img src={logoutIcon} alt="logout" className={'logout-icon'} />
        <p>Logout</p>
      </button>
    </aside>
  );
};

export default Sidebar;
