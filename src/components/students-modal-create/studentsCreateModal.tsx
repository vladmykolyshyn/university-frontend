import React from 'react';
import './studentsCreateModal.scss';
import { SubmitHandler, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import Input from '../input/input';
import { Inputs } from '../../containers/sign-in/signInForm';
import Button from '../button/button';
import closeModalImage from '../../img/CloseModal-icon.svg';
import { EntityService } from '../../services/entity.service';

interface IStudentModal {
  setVisibleModalStudent: (visible: boolean) => void;
  updateStudentsList: () => void;
}

const StudentsCreateModal: React.FC<IStudentModal> = ({
  setVisibleModalStudent,
  updateStudentsList,
}) => {
  const typeText = 'text';
  const aboveInputName = 'Name';
  const aboveInputSurname = 'Surname';
  const aboveInputEmail = 'Email';
  const aboveInputAge = 'Age';
  const placeholderName = 'John';
  const placeholderSurname = 'Smith';
  const placeholderEmail = 'name@mail.com';
  const placeholderAge = '25';
  const buttonLogin = 'btn-login';
  const buttonText = 'Create';

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name, surname, username, age } = data;
      const response = await EntityService.createStudent({
        name: name,
        surname: surname,
        email: username,
        age: Number(age),
      });
      if (response) {
        toast.success('Student is successfully created');
        setVisibleModalStudent(false);
        updateStudentsList();
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="student-modal-container">
      <main className="student-modal-main-info">
        <button
          className="student-close-modal-btn"
          onClick={() => setVisibleModalStudent(false)}
        >
          <img src={closeModalImage} alt="close" />
        </button>
        <h1>Add new student</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            type={typeText}
            placeholder={placeholderName}
            aboveInputText={aboveInputName}
            name="name"
            register={register}
            errors={errors}
            validation={{
              required: 'Name is required',
              minLength: {
                value: 3,
                message: 'Name must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Name cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderSurname}
            aboveInputText={aboveInputSurname}
            name="surname"
            register={register}
            errors={errors}
            validation={{
              required: 'Surname is required',
              minLength: {
                value: 3,
                message: 'Surname must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Surname cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderEmail}
            aboveInputText={aboveInputEmail}
            name="username"
            register={register}
            errors={errors}
            validation={{
              required: 'Email is required',
              minLength: {
                value: 5,
                message: 'Email must be at least 5 characters',
              },
              maxLength: {
                value: 30,
                message: 'Email cannot exceed 30 characters',
              },
              pattern: {
                value: /@/,
                message: "Email must contain '@'",
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderAge}
            aboveInputText={aboveInputAge}
            name="age"
            register={register}
            errors={errors}
            validation={{
              required: 'Age is required',
              minLength: {
                value: 1,
                message: 'Age must be at least 1 characters',
              },
              maxLength: {
                value: 3,
                message: 'Age cannot exceed 3 characters',
              },
            }}
          />
          <div className="student-container-button-create">
            <Button className={buttonLogin} buttonText={buttonText} />
          </div>
        </form>
      </main>
    </section>
  );
};

export default StudentsCreateModal;
