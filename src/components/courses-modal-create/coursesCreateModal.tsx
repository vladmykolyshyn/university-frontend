import React from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import './coursesCreateModal.scss';
import Input from '../input/input';
import { Inputs } from '../../containers/sign-in/signInForm';
import Button from '../button/button';
import closeModalImage from '../../img/CloseModal-icon.svg';
import { EntityService } from '../../services/entity.service';

interface ICourseModal {
  setVisibleModalCourse: (visible: boolean) => void;
  updateCoursesList: () => void;
}

const CoursesCreateModal: React.FC<ICourseModal> = ({
  setVisibleModalCourse,
  updateCoursesList,
}) => {
  const typeText = 'text';
  const aboveInputName = 'Name';
  const aboveInputDescription = 'Description';
  const aboveInputHours = 'Hours';
  const placeholderName = 'John';
  const placeholderDescription = 'Interesting';
  const placeholderHours = '97';
  const buttonLogin = 'btn-login';
  const buttonText = 'Create';

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name, description, hours } = data;
      const response = await EntityService.createCourse({
        name: name,
        description: description,
        hours: Number(hours),
      });
      if (response) {
        toast.success('Course is successfully created');
        setVisibleModalCourse(false);
        updateCoursesList();
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="course-modal-container">
      <main className="course-modal-main-info">
        <button
          className="course-close-modal-btn"
          onClick={() => setVisibleModalCourse(false)}
        >
          <img src={closeModalImage} alt="close" />
        </button>
        <h1>Add new Course</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            type={typeText}
            placeholder={placeholderName}
            aboveInputText={aboveInputName}
            name="name"
            register={register}
            errors={errors}
            validation={{
              required: 'Name is required',
              minLength: {
                value: 3,
                message: 'Name must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Name cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderDescription}
            aboveInputText={aboveInputDescription}
            name="description"
            register={register}
            errors={errors}
            validation={{
              required: 'Description is required',
              minLength: {
                value: 5,
                message: 'Description must be at least 5 characters',
              },
              maxLength: {
                value: 20,
                message: 'Description cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderHours}
            aboveInputText={aboveInputHours}
            name="hours"
            register={register}
            errors={errors}
            validation={{
              required: 'Hours is required',
              minLength: {
                value: 1,
                message: 'Hours must be at least 1 characters',
              },
              maxLength: {
                value: 30,
                message: 'Hours cannot exceed 30 characters',
              },
            }}
          />
          <div className="course-container-button-create">
            <Button className={buttonLogin} buttonText={buttonText} />
          </div>
        </form>
      </main>
    </section>
  );
};

export default CoursesCreateModal;
