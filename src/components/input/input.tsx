import React from "react";
import "./input.scss"
import { Inputs } from "../../containers/sign-in/signInForm";
import { UseFormRegister } from "react-hook-form";

interface InputProps {
  aboveInputText: string;
  type: string;
  placeholder: string;
  name: keyof Inputs;
  register: UseFormRegister<Inputs>;
  validation?: Record<string, any>;
  errors?: Record<string, any>;
}

const Input: React.FC<InputProps> = ({ aboveInputText, type, placeholder, name, register, validation, errors }) => {
  const inputId = "input-" + aboveInputText.toLowerCase().replace(/\s/g, "-");

  return (
    <div className="input-container">
      <label className="input-label" htmlFor={inputId}>
        <p className="text-above">{aboveInputText}</p>
      </label>
      <input
        autoComplete="off"
        className="input-field"
        id={inputId}
        type={type}
        placeholder={placeholder}
        {...register(name, validation)}
      />
      {errors && errors[name] && (
        <p className="error-message">{errors[name].message}</p>
      )}
    </div>
  );
};

export default Input;
