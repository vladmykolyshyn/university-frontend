import React from "react";
import "./headerId.scss";
import headerLogo from '../../img/Header-logo.svg'
import avatar from "../../img/Avatar.svg";

const HeaderDetail: React.FC = () => {
  return (
    <header className="header-container">
      <img className="header-logo-img" src={headerLogo} alt="logo" />
      <img className="header-avatar-img" src={avatar} alt="avatar" />
    </header>
  );
};

export default HeaderDetail;
