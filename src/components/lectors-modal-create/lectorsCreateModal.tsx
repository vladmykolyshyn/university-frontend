import React from 'react';
import './lectorsCreateModal.scss';
import { SubmitHandler, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import Input from '../input/input';
import { Inputs } from '../../containers/sign-in/signInForm';
import Button from '../button/button';
import closeModalImage from '../../img/CloseModal-icon.svg';
import { EntityService } from '../../services/entity.service';

interface ILectorModal {
  setVisibleModalLector: (visible: boolean) => void;
  updateLectorsList: () => void;
}

const LectorsCreateModal: React.FC<ILectorModal> = ({
  setVisibleModalLector,
  updateLectorsList,
}) => {
  const typeText = 'text';
  const aboveInputName = 'Name';
  const aboveInputSurname = 'Surname';
  const aboveInputEmail = 'Email';
  const aboveInputPassword = 'Password';
  const confirmInputPassword = 'Confirm Password';
  const placeholderName = 'John';
  const placeholderSurname = 'Smith';
  const placeholderEmail = 'name@mail.com';
  const placeholderPassword = 'password';
  const buttonLogin = 'btn-login';
  const buttonText = 'Create';

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    try {
      console.log(data);
      const { name, surname, username, password } = data;
      const response = await EntityService.createLector({
        name: name,
        surname: surname,
        email: username,
        password: password,
      });
      if (response) {
        toast.success('Lector is successfully created');
        setVisibleModalLector(false);
        updateLectorsList();
      }
    } catch (err: any) {
      const error = err.response?.data.message;
      toast.error(error.toString());
    }
  };

  return (
    <section className="lector-modal-container">
      <main className="lector-modal-main-info">
        <button
          className="lector-close-modal-btn"
          onClick={() => setVisibleModalLector(false)}
        >
          <img src={closeModalImage} alt="close" />
        </button>
        <h1>Add new lector</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            type={typeText}
            placeholder={placeholderName}
            aboveInputText={aboveInputName}
            name="name"
            register={register}
            errors={errors}
            validation={{
              required: 'Name is required',
              minLength: {
                value: 3,
                message: 'Name must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Name cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderSurname}
            aboveInputText={aboveInputSurname}
            name="surname"
            register={register}
            errors={errors}
            validation={{
              required: 'Surname is required',
              minLength: {
                value: 3,
                message: 'Surname must be at least 3 characters',
              },
              maxLength: {
                value: 20,
                message: 'Surname cannot exceed 20 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderEmail}
            aboveInputText={aboveInputEmail}
            name="username"
            register={register}
            errors={errors}
            validation={{
              required: 'Email is required',
              minLength: {
                value: 5,
                message: 'Email must be at least 5 characters',
              },
              maxLength: {
                value: 30,
                message: 'Email cannot exceed 30 characters',
              },
              pattern: {
                value: /@/,
                message: "Email must contain '@'",
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderPassword}
            aboveInputText={aboveInputPassword}
            name="password"
            register={register}
            errors={errors}
            validation={{
              required: 'Password is required',
              minLength: {
                value: 5,
                message: 'Password must be at least 5 characters',
              },
              maxLength: {
                value: 100,
                message: 'Password cannot exceed 30 characters',
              },
            }}
          />
          <Input
            type={typeText}
            placeholder={placeholderPassword}
            aboveInputText={confirmInputPassword}
            name="confirmPassword"
            register={register}
            errors={errors}
            validation={{
              required: 'Confirm Password is required',
              validate: (value: string) =>
                value === getValues('password') || 'Passwords do not match',
            }}
          />
          <div className="lector-container-button-create">
            <Button className={buttonLogin} buttonText={buttonText} />
          </div>
        </form>
      </main>
    </section>
  );
};

export default LectorsCreateModal;
