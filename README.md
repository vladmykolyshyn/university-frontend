# Front-end of project: "University"
- For full implementation use back-end of this project - https://gitlab.com/vladmykolyshyn/nestjs-university
- After starting project you can sign-in; sign-up and reset-password
- Logic for reset password - for sending letter I use my mail and password from outlook mail. And I receive all letters with restoration's password link in mailinator.com - https://www.mailinator.com/
- If you want use this service also you should create your users(lectors) with email ...@mailinator.com
- After authentication - you can see all lectors, students, courses and groups, which you have in DataBase
- You can add or edit these enities
- One student can have one group, but many courses
 




